#!/usr/bin/env python3
import importlib.util
import markdown
import sys

# Local file for configuring various things
import local_settings

if __name__ == "__main__":
    page_spec = sys.argv[1]

    module_spec = importlib.util.spec_from_file_location("page", page_spec)
    module = importlib.util.module_from_spec(module_spec)
    module_spec.loader.exec_module(module)

    sys.stdout.buffer.write(f'''<html>
    <head>
        <link rel="stylesheet" href="{local_settings.stylesheet}">
        <title>{module.page_title}</title>
    </head>
    <body>
'''.encode("utf8"))
    #Insert converted markdown contents for body
    markdown.markdownFromFile(input = open(module.page_file, "r+b"), output = sys.stdout.buffer)
    sys.stdout.buffer.write(b'''
    </body>
</html>''')
        

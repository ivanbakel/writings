# Why your configuration files should be code.

### 1. Configurations
Good applications are configurable. They try to anticipate user desires, rather than just use cases, and give the options to make the application behave as close to any one of many needs as possible. Good configurations highlight good encapsulation, good dependency injection, and good practices - the better these principles are followed, the more a user can do to fit what they want to do.

Good configurations are powerful. They allow for automation, for compatibility with third-party tools, for case-by-case changes without the need for global fiddling. The best experience with a new tool is being able to slide it into an existing setup and have it work exactly as you expect - because the configuration gives you that power. An application's configuration is an extension of its API, and like all APIs, it benefits from having the features to do many things with few concepts.

Configuration abstraction is dangerous. The Unix philosophy of "everything is text" allows for good integration between various systems, so long as they don't do anything unusual. Static configurations can use this kind of existing infrastructure to try to make themselves more dynamic: need a value varying by time of day? Hook a script up to it, and set your configuration value to a command to run it. The user has been given power, and the configuration system hasn't had to become more complex, but this interop layer is flimsy : what ties the script to the configuration in a way that one always follows the other? What happens if the configuration API suddenly changes? Text isn't formalised, or verified.

### 2. The Inner Platform Effect
In trying to make their systems more configurable, some developers can fall into the trap of trying to make user choices expressible through configuration combinations. This commonly manifests in simple concepts like whitelist/blacklist combinations, but it can easily spiral into extreme cases, such as conditionals built out of configuration objects e.g.


    <And>
        <Not>
	       ...
        </Not>
        ...
    </And>

In becoming more powerful, these configuration systems come to resemble the languages that they were originally written in. They lose the value of a simple configuration, easily manipulated by any user, and approach the complexity and required learning of typical code. Worse still, these lack the important features that languages have attained by design or experience, or the ecosystems that grow alongside them: the imitation is sloppy, difficult to learn, and lacking in true support for the end user.

### 3. Code
Programming languages are effectively expressors for massively-configurable systems. They transform static files of code - text describing abstract desires - into real behaviour. To the application of the compiler, the programming language is the ultimate configuration - it allows for any kind of behaviour, to exactly the user specifications, with no assumptions. A language represents the best kind of API - it distills its concepts into a single, tightly-defined core, and extends their possible uses into the infinite.

Programming languages have strong interoperability. Strongly typed languages enforce restrictions on value types changing. Languages which compose programs out of multiple files prevent the dispersal of each part from the whole - every move of an individual piece is either conscious or caught.

To varying degrees, programming languages are well-defined enough to catch user errors in the attempt, rather than after the fact. Furthermore, they have the tools and community knowhow to support learning and problem-solving in common, context-independent ways - the benefit of this will be apparent to anyone who has had to approach an issue by relying on the knowledge of others. In their generality, languages can unify issues into common problems, and give those problems consistent solutions.

### 4. Combination
With a use case and a utility, the fusion of the two - and the point I want to make - now seems to be obvious: every need for a configuration system should be using a programming language to express it. Languages are designed for user power combined with ease of use, and configuration systems which give the user power easily are the best kind.

The only trouble is that it's not that simple. As developers, it's easy to think of tools as being powerful independently of a learning curve - what matters most is how forgiving it is of mistakes, and how easy it is to catch them. To the extreme, we find that interest in new technologies can encourage exciting new ventures in development, at least in part encouraged by the desire to learn more about a new tool, with a strong belief in learning by doing. The danger is projecting this almost academic viewpoint onto the end-user.

People want tools that work. This isn't to say that developers are somehow willing to forgive failure, but they can accept the burden of having to do work themselves much more easily. It's easy to see that this is a natural outcome of using a language in the first place - in the most general system, goals are too varied to be railroadable. Being able to do everything necessitates learning how to do anything. But when this is extended to systems with specific intentions, it's harder to justify: the more obvious it is what you ought to be trying to do, the less you'd accept having to express how to do it yourself. Tools that work strike this balance.

The end-user's job is not to code. In a sense, this is why they are the app's end-user - the relationship between the programmer and the user should be to provide a product for which the programmer has done the coding and the user will do the using. On the other hand, this doesn't mean that the user should be unable to code. One of the main principles of the Free Software movement is to grant these freedoms to every user, regardless of whether or not the majority of them will use it. A tool which works out of the box is not necessarily better for denying this possibility.

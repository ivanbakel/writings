# Signature #
## COP Functional Language ##

## Intro ##

_Signature_ is a functional contract-oriented programming language. It defines both a model of the language systems and their syntax implementation.

Code in _Signature_ is made up of type definitions, binding definitions, metacode, and the input binding. The input binding is a special binding which forms the context of the running program.

Expressions in _Signature_ are executed in a _context_, which is a monad from which values are taken. _Signature_ is a "forward-only" language, meaning every expression is the context for the next - expressions with no value do not exist. 

## Type Specification ##

### Types ###

_Signature_ uses a recursive type system, with three main supertypes - **contracts**, **objects**, and **definitions** - and several special value types. Each type can take none or more _type arguments_ according to the **argument** system. The special types are:

 * _Type_ : The superclass of all types. _Type_ is a definition, and takes type arguments using `[(Type...)...]`. All implemented types are subclasses of _Type_, with the type arguments being the type argument restrictions of the implemented type, if any. 
 * _Tuple_ : A constructed type with type arguments `[Type...]`, written as `(Type...)`. Tuples may be constructed with arbitrary type arguments. The tuple with no type arguments is called the _empty tuple_. The tuple with a single type argument is considered identical in type to its single argument - they can be used interchangeably.
 * _Context_ : A _named tuple_, the Context type is used to store and retrieve **bindings** and types by name. It is similar to a tuple in being constructed with arbitrarily many types, but differs in that values are accessed by a well-typed compile-time name, not by position.
 * _List_ : A collection of multiple values with type arguments `[Type]`, written as `{Type}`. Lists may contain subtypes of their argument type.  Lists may be empty.

### Arguments ###

Types take arguments through the _argument system_, a set of rules for defining nesting and interpretation. The type arguments for a type are denoted by a comma-delimited set of arguments in square brackets.

Each argument is represented by a **type identifier**. Every type identifier may be given an _alias_, which refers to that identifier, and may be used in place of the identifier. In square-brackets notation, an alias is denoted by wrapping a type identifier in round brackets, and writing the alias before it i.e. `alias(identifier)`. An alias may contain itself, and an alias which only contains itself is equivalent to Type, and may simply be written as the the alias with empty brackets. If an alias is not re-used, it is identical in type to the identifier it is an alias for i.e. `a()` is equivalent to `Type`.

 The basic type identifier is made out of zero or more contract types or nested type identifiers. All contracts with type arguments used in a single identifer must have all their type arguments _qualified_, by passing one or more existing aliases or concrete types using angle brackets, in the order of the contract's type arguments i.e. `contract<alias or type>`. Contracts in an identifier are separated by colons i.e. `contract:contract`.

Any type identifier may also be extended to indicate that zero or more are accepted. In square-bracket notation, this is done through the plural operator - three dots - after the type identifier i.e. `identifer...` . This extension itself produces a valid type identifier - the plural operator has type `[Type]`. The plural operator is _greedy_, meaning as many types in its position which fulfill its requirements are taken to be part of the argument as possible. The plural operator may also be followed by a pair of numbers indicating the minimum or maximum number of types taken, in round brackets i.e. `(a, b)`. If the first number is omitted, it is assumed to be 0; if the second is omitted, it is assumed to be unbounded. 

#### Examples ####

 * The **function** type accepts an arbitrary number of input types, and gives a single output type - in simple terms, it has arguments `[result(), inputs(Type...)]`, and is written as `inputs(Type...) -> result()`.
 * The **join** type also accepts an arbitrary number of input types, and also the joined-on type, and returns a tuple of the joined type and the output type. Joins have arguments `[jointype(Object), returntype(), inputs(Type...)]`, written as `jointype(Object), inputs(Type...) -< (jointype, result())` - note that the right-hand side is a partially-qualified tuple using `jointype`. In cases where the object being joined on is known, the redundancy is omitted to give just `inputs(Type...) -< result()`.
 * It is possible to be more specific about functions and joins. We can define the type of all two-argument functions as `a(), b() -> c()` - they have two input types (which may be the same) and a single return type. Equally, we could write this as `Type...(2, 2) -> Type` using the plural operator's quantifier feature, or simply `Type, Type -> Type` - all are identical.
 * We can imagine an object with a single type argument, which stored either a **List** with that type, or a single value of that type. Its type arguments would then be `[a()]`, and the values it could store would have type `{a}` (or `List<a>`, the List type qualified by the alias `a`) or just `a`. 

#### Meta-types ####

The type of a **basic type identifier** is `[Type...]`, as it accepts an unlimited number of contract types. The plural operator has type `Type..., Natural, Natural -> Type...` : given an existing identifier and two number bounds, it produces a new type identifier. 

An alias for some type identifiers has the type of a tuple of the types of those identifiers - therefore, if the alias is of a single type identifier, the alias shares its type. The _alias operator_ has type `input((Type...)...) -> (input)`.

The type of a type with 2 type arguments is `Type..., Type... -> Type` - it accepts two type identifiers and produces a new type.

### Contracts ###
A **contract** is a collection of **bindings**, either declared or implemented. Contracts are implemented by **objects**, which must provide implementations for all bindings the contract declares, and may provide implementations for already-implemented bindings as with object inheritance. Contracts may also implement other contracts, but do not have to provide implementations for their declared bindings, though they may do so.

A **definition contract** is a contract which declares or implements only **definition bindings**. Definitions may only implement definition contracts. **Objects** may implement definition contracts. Contracts which declare or implement at least one non-definition binding are known as **object contracts** for this reason.

### Objects ###

An **object** is an instantiable collection of **bindings** which implements one or more **contracts** - strictly, it may implement none, but it would then be useless.

Objects have their own scope, and a unique keyword - _create_. When used, this keyword instantiates a new copy of the object in memory, and its return value is the object created in this way, after the special _creation_ **join** is run.

Unlike object-oriented languages, objects cannot inherit from other objects - Signature promotes composition over inheritance by allowing an object to _promote_ bindings held in **properties** to act as bindings on the object itself. The type signature of a **function** promoted in this way is unchanged, but **join** bindings change from `(property_type, return_type)[input_types(Type...)]` to `(object_type, return_type)[input_types(Type...)]`.

### Definitions ###

A **definition** is a non-instantiable collection of **bindings** which implement one or more **contracts**, similar to objects. Unlike objects, definitions may only implement **definition bindings** and **definition contracts** - as such, they may take no other type arguments.

Definitions have only one instance, which is created automatically before use,and occupy a fixed space in memory.

#### Enums ####

**Enumerations** or _enums_ are a special case of definitions. They allow for an automatic way of creating a contract which allows for central access to all its implementations, none of which are extensible. Each **enumeration value** is required to implement the same contracts and bindings, all decided by the parent **enumeration contract**.
 
### Bindings ###

A **binding** is a description of behaviour on a **type**, which can be either declared or initialised. Bindings have the type `[Type...]` and may make use of the type arguments of their containing type, as well as having their own type arguments. Bindings all have at least one type argument - the value of their full evaluation. 

Bindings are made up of **functions**, and **joins** - definitions may only implement functions, and for this reason they are called **definition bindings**.

### Functions ###

A **function** is a mapping from one type onto another with at least one argument and a return value. It has type-argument type `Type... -> Type`.

 Functions may not access or change the state of their enclosing type, and while functions _may_ return the empty tuple, as _Signature_ is forward-only, this restricts the calling code to also return the empty tuple, or a constant value - it is not recommended. 

Functions with only one type argument i.e. the return type, are known as **constant functions**.

Functions may not call **joins**, with the exception of the special _creation_ join through the use of _create_.

### Joins ###

A **join** is similar to a function, with two main differences - joins may access and change state on their enclosing type, and the join return type is not returned directly - it is instead wrapped in a tuple, where the first value is the enclosing type instance or definition, and the second value is the return value of the join: this gives it the type `(Type, Type)[Type...]`, or `Type... -< (Type, Type)`.

 It is sometimes recommended for joins to return the empty tuple, as the calling code still has access to the enclosing type instance. 

One special instance of a join is the _creation_ join, which has type signature (T, ())[], and is called once per use of _create_ in T's object code. The object returned is the first value of the tuple resulting from calling _creation_.

### Properties ###

Objects, in implementing their _state_, can implement storage using **properties**, which are type features similar to bindings - though they may not be declared or implemented by a contract. Each **property** is a sub-object which has _getter_ and _setter_ **join(s)**, as well as the ability to record a single value of any type. For utility, properties also implement a _modify_ **join**, which allows for in-place value modification.

Each property may be declared as **global**, in which case its instance is shared between all objects - it is, however, accessed identically to a non-global property.

## Control statements ##

### Conditional ###

A **conditional** statement is an expression of the type `(Type)[Type -> Bool, Type, Type]`, which accepts two bindings of the same type and a boolean function, giving the result of the first if the function evaluates to true, and the second otherwise.

### Loop ###

A **loop** statement is a repeated conditional of the type `(Type)[Type -> Bool, Type -> Type]`, which accepts a single binding and a check if the current values should cause the loop to complete. The check is executed before each loop execution - if the result is true, the loop is executed, and otherwise, it exits.

 


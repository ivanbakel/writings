from os import path

page_title = "Signature - A Language Specification"
page_file = path.join(path.dirname(__file__), "signature.md")

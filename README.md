A git repository used to store and edit my writings, typically in Markdown.

The hosted html can typically be accessed from my personal page: https://www.doc.ic.ac.uk/~iv915

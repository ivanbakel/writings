PAGE_DIRS = Technical
PAGE_FILES = $(shell find $(PAGE_DIRS) -type f -name "*.py")
PAGE_OUT = $(PAGE_FILES:py=html)

vpath %.py $(PAGE_DIRS)

%.html : %.py
	@mkdir -p out/$(<D)
	./construct_webpage.py $< > out/$@

.PHONY = all clean

all: $(PAGE_OUT)

clean:
	rm -r $(wildcard out/*)
